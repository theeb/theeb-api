<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$api = app('Dingo\Api\Routing\Router');
$router->get('/', function () use ($router) {
        return response()->json(['status' => 'ok', 'message' => 'welcome']);
});

$api->version('v1', function ($api) {
    $api->group(['namespace' => 'App\Http\Controllers\V1', 'prefix' => '{lang}', 'middleware' => 'language'], function() use($api) {

        $api->post('/login',[
                'as' => 'user.login',
                'uses' => 'AuthController@login',
        ]);

        $api->get('/profile',[
                'as' => 'user.profile',
                'uses' => 'UserController@profile',
                'middleware' => 'jwt.auth'
        ]);

        $api->post('/logout',[
                'as' => 'user.logout',
                'uses' => 'AuthController@logout',
                'middleware' => 'jwt.auth'
        ]);

        $api->post('/dashboard',[
                'as' => 'user.dashboard',
                'uses' => 'DashboardController@dashboard',
                'middleware' => 'jwt.auth'
        ]);

        $api->post('/register', [
                'as' => 'user.register',
                'uses' => 'AuthController@register',
                'middleware' => ['jwt.auth', 'role:admin']
        ]);

        $api->get('/roleRightsList',[
                'as' => 'role.roleRights',
                'uses' => 'RolesController@roleRightsList',
                'middleware' => ['jwt.auth', 'role:admin']
        ]);

        $api->post('/createRole',[
                'as' => 'role.createRole',
                'uses' => 'RolesController@createRole',
                'middleware' => ['jwt.auth', 'role:admin']
        ]);

        $api->get('/listUsers/[{group}]', [
                'as' => 'user.listUsers',
                'uses' => 'UserController@listUsers',
                'middleware' => ['jwt.auth', 'role:admin,agent']
        ]);

        $api->get('/viewAgent', [
                'as' => 'user.viewAgent',
                'uses' => 'AgentsController@viewAgent',
                'middleware' => ['jwt.auth', 'role:admin']
        ]);

        $api->put('/updateUser/{id}', [
                'as' => 'user.updateUser',
                'uses' => 'UserController@updateUser',
                'middleware' => ['jwt.auth', 'role:admin']
        ]);

        $api->delete('/deleteUser/{id}', [
                'as' => 'user.deleteUser',
                'uses' => 'UserController@deleteUser',
                'middleware' => ['jwt.auth', 'role:admin']
        ]);

        $api->post('/customerRequest', [
                'as'    => 'user.customerRequest',
                'uses'  => 'CustomerRequestController@customerRequest',
                'middleware' => ['jwt.auth', 'role:admin']
        ]);

        $api->get('/role', [
                'as'    => 'user.role',
                'uses'  => 'AuthController@role',
                'middleware' => ['role:admin,agent']
        ]);

    });
});