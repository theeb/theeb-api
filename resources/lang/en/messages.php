<?php
    return [
        'VALIDATION_ERROR'      =>  __('request validation errors'),
        'ROLE_CREATED'          =>  __('role created successfully'),
        'ROLE_LISTED'           =>  __('role listed successfully'),
        'INVALID_REQUEST'       =>  __('invalid request'),
        'UNAUTHORIZED_ACCESS'   =>  __('unauthorized access'),
        'DASHBOARD_CONTENT'     =>  __('dashboard data listed'),
        'LOGIN_SUCCESS'         =>  __('you have successfully logged in'),
        'LOGOUT_SUCCESS'        =>  __('you have successfully logged out'),
        'USER_CREATED'          =>  __('user created successfully'),
        'USER_UPDATED'          =>  __('user updated successfully'),
        'USER_UPDATE_FAILURE'   =>  __('error while updating user'),
        'USER_DELETED'          =>  __('user deleted successfully'),
        'USER_DELETE_FAILURE'   =>  __('error while deleting user'),
        'PROFILE_LISTED'        =>  __('user profile details'),
        'AGENTS_LISTED'         =>  __('agents listed successfully'),
        'AGENT_PROFILE_LISTED'  =>  __('agent profile listed'),
        'AGENT_PROFILE_NOT_AVAILABLE'   =>  __('agent profile not available'),
        'AGENT_PROFILE_UPDATED' =>  __('agent profile updated successfully'),
        'AGENT_PROFILE_UPDATE_FAILURE' => __('error while updating agent profile'),
        'NO_AGENTS_FOUND'       =>  __('no agents found'),
        'USERS_LISTED'          => __('users listed successfully'),
        'ROLE_RIGHTS_LISTED'    => __('role rights listed successfully'),
        'NO_USERS_FOUND'        => __('no users found'),
    ];
?>