<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class UserGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_groups')->insert([
            [
                'name'      =>  'Admin',
                'slug'      =>  'admin',
                'status'    =>  1
            ],
            [
                'name'      =>  'Driver',
                'slug'      =>  'driver',
                'status'    =>  1
            ],
            [
                'name'      =>  'Agent',
                'slug'      =>  'agent',
                'status'    =>  1
            ],
            [
                'name'      =>  'Customer',
                'slug'      =>  'customer',
                'status'    =>  1
            ]
        ]);
    }
}
