<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TheebRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name'      =>  'Customer',
                'status'    =>  1
            ],
            [
                'name'      =>  'Admin',
                'status'    =>  1
            ],
            [
                'name'      =>  'Agent',
                'status'    =>  1
            ],
            [
                'name'      =>  'Driver',
                'status'    =>  1
            ]
        ]);
    }
}
