<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCustomerRequestsStageColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_requests', function (Blueprint $table) {
            $table->dropColumn('request_stage');
        });

        Schema::table('customer_requests', function (Blueprint $table) {
            $table->string('request_stage')->after('pickup_address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_requests', function (Blueprint $table) {
            $table->dropColumn('request_stage');
        });

        Schema::table('customer_requests', function (Blueprint $table) {
            $table->integer('request_stage')->after('pickup_address')->default(1);
        });
    }
}
