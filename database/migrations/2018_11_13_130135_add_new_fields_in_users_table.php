<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('last_name')->after('name')->nullable();
            $table->string('driver_code')->after('status')->nullable();
            $table->string('driver_status')->after('status')->nullable();
            $table->string('id_type')->after('status')->nullable();
            $table->string('id_number')->after('status')->nullable();
            $table->string('stop_list')->after('status')->nullable();
            $table->string('license_number')->after('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('last_name');
            $table->dropColumn('driver_code');
            $table->dropColumn('driver_status');
            $table->dropColumn('id_type');
            $table->dropColumn('id_number');
            $table->dropColumn('stop_list');
            $table->dropColumn('license_number');
        });
    }
}
