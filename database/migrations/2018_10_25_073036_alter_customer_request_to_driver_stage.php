<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCustomerRequestToDriverStage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_request_to_driver', function (Blueprint $table) {
            $table->dropColumn('driver_stage');
        });

        Schema::table('customer_request_to_driver', function (Blueprint $table) {
            $table->string('driver_stage')->after('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_request_to_driver', function (Blueprint $table) {
            $table->dropColumn('driver_stage');
        });

        Schema::table('customer_request_to_driver', function (Blueprint $table) {
            $table->integer('driver_stage')->after('status')->nullable();
        });
    }
}
