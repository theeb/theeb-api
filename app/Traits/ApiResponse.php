<?php

namespace App\Traits;
use Illuminate\Http\Response;
trait ApiResponse
{
    public function sendResponse($statusCode = 403, $data = [], $message = null)
    {
        if( empty($message) )
            $message = config('constants.UNAUTHORIZED_ACCESS');

        return Response::create([
            'data'      => $data,
            'message'   => $message
        ], $statusCode);
    }
}