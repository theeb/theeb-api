<?php 
namespace App\Repositories\Contracts\V1;

interface UserRepositoryInterface
{
     /**
     * @return mixed
     */
    public function entity(): string;
}