<?php 
namespace App\Repositories\Contracts\V1;

interface UserRolesRepositoryInterface
{
     /**
     * @return mixed
     */
    public function entity(): string;
}