<?php 
namespace App\Repositories\Contracts\V1;

interface CustomerRequestsRepositoryInterface
{
     /**
     * @return mixed
     */
    public function entity(): string;
}