<?php 
namespace App\Repositories\Contracts\V1;

interface RoleRepositoryInterface
{
     /**
     * @return mixed
     */
    public function entity(): string;
}