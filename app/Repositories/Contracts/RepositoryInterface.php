<?php

namespace App\Repositories\Contracts;

/**
 * Interface RepositoryInterface.
 */
interface RepositoryInterface
{
    

    /**
     * @return mixed
     */
    public function all();

    /**
     * @param array ...$recordId
     *
     * @return mixed
     */
    public function find(...$recordId);

    /**
     * @param $recordId
     *
     * @return mixed
     */
    public function findFirst($recordId);

    /**
     * @param $column
     * @param $value
     *
     * @return mixed
     */
    public function findWhere($column, $value);

    /**
     * @param $column
     * @param $value
     *
     * @return mixed
     */
    public function findWhereFirst($column, $value);

    /**
     * @param array $properties
     *
     * @return mixed
     */
    public function create(array $properties);

    /**
     * @param $recordId
     * @param array $properties
     *
     * @return mixed
     */
    public function update($recordId, array $properties);

    /**
     * @param $recordId
     *
     * @return mixed
     */
    public function delete($recordId);

    /**
     * @param int  $perPage
     * @param null $appends
     *
     * @return mixed
     */
    public function paginate($perPage = 10, $appends = null);
}
