<?php 
namespace App\Repositories\Eloquent\V1;

use App\Models\V1\CustomerRequests;
use App\Repositories\Repository;
use App\Repositories\Contracts\V1\CustomerRequestsRepositoryInterface;

class CustomerRequestsRepository extends Repository implements CustomerRequestsRepositoryInterface
{

   /**
     * @return string
     */
    public function entity(): string
    {
        return CustomerRequests::class;
    }
}