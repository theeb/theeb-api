<?php 
namespace App\Repositories\Eloquent\V1;

use App\Models\V1\Role;
use App\Repositories\Repository;
use App\Repositories\Contracts\V1\RoleRepositoryInterface;

class RoleRepository extends Repository implements RoleRepositoryInterface
{

   /**
     * @return string
     */
    public function entity(): string
    {
        return Role::class;
    }
}