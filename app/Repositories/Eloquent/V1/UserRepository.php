<?php 
namespace App\Repositories\Eloquent\V1;

use App\Models\V1\User;
use App\Repositories\Repository;
use App\Repositories\Contracts\V1\UserRepositoryInterface;
use Illuminate\Support\Facades\DB;

class UserRepository extends Repository implements UserRepositoryInterface
{

    private $model = null;
    private $pfx = "";
   
    public function entity(): string
    {
        $this->model = User::class;
        $this->pfx = DB::getTablePrefix();
        return User::class;
    }

    public function getListInAdminDashboard($withTrash = false)
    {
        $dashboard =  DB::table('users')
        ->join('users_roles', 'users.id', '=', 'users_roles.user_id')
        ->join('roles', 'roles.id', '=', 'users_roles.role_id')
        ->join('user_groups', 'user_groups.id', '=', 'roles.group_id')
        ->select('user_groups.id',  'user_groups.name', 'user_groups.slug as group',  DB::raw("COUNT(". $this->pfx."users.id) AS total_users"));
        
        return $dashboard->groupBy("user_groups.id")->get()->toArray();
    }

    public function viewUserDetails($userSlug = '', $userId = 0)
    {
        return User::where('users.id' , $userId)
            ->with('userRole.role.groups')
            ->whereHas('userRole.role.groups', function($query) use($userSlug) {
                if( !empty($userSlug) )
                    $query->where('user_groups.slug', $userSlug);
            });
    }
}