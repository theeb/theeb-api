<?php 
namespace App\Repositories\Eloquent\V1;

use App\Models\V1\UsersRoles;
use App\Repositories\Repository;
use App\Repositories\Contracts\V1\UserRolesRepositoryInterface;

class UserRolesRepository extends Repository implements UserRolesRepositoryInterface
{

   /**
     * @return string
     */
    public function entity(): string
    {
        return UsersRoles::class;
    }
}