<?php

namespace App\Repositories;

use App\Exceptions\NoEntityException;
use App\Repositories\Contracts\RepositoryInterface;

/**
 * Class Repository.
 *
 * @SuppressWarnings(PHPMD)
 */
abstract class Repository implements RepositoryInterface
{
    /**
     * @var \Laravel\Lumen\Application|mixed
     */
    public $entity;

    /**
     * Repository constructor.
     *
     * @throws NoEntityException
     */
    public function __construct()
    {
        $this->entity = $this->resolveEntity();
    }
    
    /**
     * @throws \Illuminate\Container\EntryNotFoundException
     *
     * @return mixed
     */
    public function all()
    {
        return $this->entity->get();
    }


    /**
     * @throws \Illuminate\Container\EntryNotFoundException
     *
     * @return mixed
     */
    public function count()
    {
        return $this->entity->get()->count();
    }

    /**
     * @throws \Illuminate\Container\EntryNotFoundException
     *
     * @return mixed
     */
    public function countWhere($args)
    {
        return $this->entity->where($args)->get()->count();
    }

    /**
     * @param array ...$recordId
     *
     * 
     *
     * @return mixed
     */
    public function find(...$recordId)
    {
        $data = $this->entity->find(...$recordId);
       
        return $data;
    }

    /**
     * @param $recordId
     *
     * 
     *
     * @return mixed
     */
    public function findFirst($recordId)
    {
        $data = $this->entity->find($recordId);
        return $data;
    }

    /**
     * @param $column
     * @param $value
     *
     * @return mixed
     */
    public function findWhere($column, $value)
    {
        return $this->entity->where($column, $value)->get();
    }

    public function findWhereArray($whereArray)
    {
        return $this->entity->where($whereArray);
    }


    /**
     * @param $column
     * @param $value
     *
     * @return mixed
     */
    public function findWhereFirst($column, $value)
    {
        return $this->entity->where($column, $value)->first();
    }

    /**
     * @param array $properties
     *
     * @return mixed
     */
    public function create(array $properties)
    {
        return $this->entity->create($properties);
    }

    /**
     * @param $recordId
     * @param array $properties
     *
     * @throws NoRecordsException
     *
     * @return mixed
     */
    public function update($recordId, array $properties)
    {
        $data = $this->entity->find($recordId);
        if($data)
            return $data->update($properties);
        else
            return false;
    }




    /**
    * @param array $where
    * @param array $properties
    *
    * @throws NoRecordsException
    *
    * @return mixed
    */
    public function updateWhere(array $where, array $properties)
    {
        return $this->entity->where($where)
            ->update($properties);

    }

    /**
     * @param $recordId
     *
     * 
     *
     * @return mixed
     */
    public function delete($recordId)
    {
        $data = $this->entity->find($recordId);
        if($data)
            return $data->delete();
        else
            return false;
    }


    /**
     * @param int  $perPage
     * @param null $appends
     *
     * 
     *
     * @return mixed
     */
    public function paginate($perPage = 10, $appends = null)
    {
        $data = $this->entity->paginate($perPage);       

        return null !== $appends ? $data->appends($appends) : $data;
    }

    /**
     * @throws NoEntityException
     *
     * @return \Laravel\Lumen\Application|mixed
     */
    private function resolveEntity()
    {
        if (!method_exists($this, 'entity')) {
            throw new NoEntityException('Entity not found', 500);
        }

        return app($this->entity());
    }

    public function __call($method, $args)
    {
        return \call_user_func_array([$this->entity, $method], $args);
    }

    public function model()
    {
        return $this->entity;
    }

    public function getFilter($filters = array()) {
        $model = $this->entity;
              
        if(isset($filters['where']) && is_array($filters['where']))
        {
         $model =  $model->where($filters['where']); 
        }
        if(isset($filters['whereIn'])&& is_array($filters['whereIn']))
        {
         $whereIn = $filters['whereIn'];
         foreach($whereIn as $key => $in)
         {
             $model = $model->whereIn($key,$in);
         }
        }
        if(isset($filters['whereBetween']) && is_array($filters['whereBetween']))
        {
         $whereBetween = $filters['whereBetween'];
             foreach($whereBetween as $key => $between)            
             {
                $model = $model->whereBetween($key,$between); 
             }
        }
        if(isset($filters['getQuery']) && ($filters['getQuery']==true))
        {
          return $model;
        } 
        else if(isset($filters['columns']) && !empty($filters['columns'])) {
              return $model->get($columns);
         } 
        else {
          return $model->get();
         }    
     }
}
