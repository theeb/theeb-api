<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class CustomerMiddleware extends Controller
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
}
