<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class LanguageMiddleware extends Controller
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->segment(2) == 'ar' ? app('translator')->setLocale('ar') : '';
        return $next($request);
    }
}
