<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class RoleMiddleware extends Controller
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ... $roles)
    {
        $userId     =   app('auth')->user()->id;
        $userRole   =   \App\Models\V1\User::getUserRole($userId);
        

        if( $userRole->group_slug == 'admin' ){
            return $next($request);
        }

        foreach ($roles as $role) {
           if( $role == $userRole->group_slug ){
            return $next($request);
           }
        }

        return $this->sendResponse($this->statusCode, $this->data, $this->message);
        
    }
}
