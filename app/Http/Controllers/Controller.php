<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Traits\ApiResponse;
use Illuminate\Http\Response;


class Controller extends BaseController
{
    //
    use ApiResponse;

    public $statusCode  =   403;
    public $data        =   [];
    public $message     =   "";

    public function __construct()
    {
        $this->statusCode =  Response::HTTP_UNAUTHORIZED;
        $this->data = [];
        $this->message = config('constants.UNAUTHORIZED_ACCESS');
    }

}
