<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required', 
            'email' => 'required|email|unique:users'
        ]);

        $user = User::create($request->all());

        return response()->json($user);

    }
}
