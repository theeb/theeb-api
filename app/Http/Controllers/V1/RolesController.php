<?php
namespace App\Http\Controllers\V1;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use App\Repositories\Contracts\V1\RoleRepositoryInterface;

use App\Models\V1\Roles;
use App\Models\V1\Permissions;
use App\Models\V1\RolePermissions;
use App\Repositories\TheebRepositories\Repository;
use Validator;
use Helpers;
use Illuminate\Http\Response;

class RolesController extends Controller
{
    private $role               = null;
    private $permissions        = null;
    private $rolePermissions    = null;

    public function __construct()
    {
        $this->role = new Repository(new Roles);
        $this->permission = new Repository(new Permissions);
        $this->rolePermissions = new Repository(new RolePermissions);
    }

    public function createRole(Request $request)
    {

        $request->merge(array_map([new Helpers(), "trimIfString"], $request->all()));
        $validator = Validator::make($request->all(),[
            'name'          => 'required|unique:roles',
            'permissions'   => 'required',
            'group_id'      => 'required'
        ]);

        if($validator->fails())
        {
            $this->statusCode = Response::HTTP_UNPROCESSABLE_ENTITY;
            $this->message =  trans('messages.VALIDATION_ERROR');
            $this->data = ['error'  => $validator->errors()];
        }else{
            $request->merge(['slug' => str_slug($request->name,'-')]);
            $role = $this->role->create($request->only(['name', 'slug', 'group_id']))->toArray();
            
            $rolePermissions = [];
            foreach ($request->permissions as $key => $permission_id) {
                $rolePermissions[] = [
                    'role_id'           =>  $role['id'],
                    'permission_id'     =>  $permission_id
                ];
            }

            $role['role_permissions'] = $this->rolePermissions->insert( $rolePermissions );

            $this->statusCode = Response::HTTP_CREATED;
            $this->message =  trans('messages.ROLE_CREATED');
            $this->data = ['role' => $role];
        }

        return $this->sendResponse($this->statusCode, $this->data, $this->message);
    }

    public function roleRightsList()
    {
        return $this->sendResponse(Response::HTTP_OK, ['rights' => $this->permission->all()], trans('messages.ROLE_RIGHTS_LISTED'));
    }

}
?>