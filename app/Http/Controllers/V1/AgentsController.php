<?php
namespace App\Http\Controllers\V1;


use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Contracts\V1\UserRepositoryInterface;
use App\Models\V1\User;
use Validator;
use Illuminate\Http\Response;

class AgentsController extends Controller
{
    private $user = null;
    private $userGroups = null;

    const AGENT_SLUG = 'agent';

    public function __construct()
    {
        $this->user = app(UserRepositoryInterface::class);
    }

    public function listAgents(Request $request)
    {
        $agents = $this->user->model()->with('userRole.role.groups')->whereHas('userRole.role.groups', function($query){
            $query->where('user_groups.slug', self::AGENT_SLUG);
        })->paginate(config('constants.PAGINATION_PER_PAGE'));

        $message = trans('messages.NO_AGENTS_FOUND');
        if($agents->count()){
            $message = trans('messages.AGENTS_LISTED');
        }

        return $this->sendResponse(Response::HTTP_OK, ['agents' => $agents], $message);
    }

    public function viewAgent(Request $request)
    {
        $agent = $this->user->viewUserDetails(self::AGENT_SLUG,$request->get('userid'));
        if($agent->count()){
            $message = trans('messages.AGENT_PROFILE_LISTED');
        }else{
            $message = trans('messages.AGENT_PROFILE_NOT_AVAILABLE');
        }
        return $this->sendResponse(Response::HTTP_OK, ['agent' => $agent->first()], $message);
    }

}
?>