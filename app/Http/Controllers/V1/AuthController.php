<?php 
namespace App\Http\Controllers\V1;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Contracts\V1\UserRepositoryInterface;
use App\Repositories\Contracts\V1\UserRolesRepositoryInterface;
use Validator;
use App\Models\V1\User;
use Illuminate\Http\Response;

class AuthController extends Controller
{
    private $userRepo;
    private $userRoles;
    const EXPIRY_TIME_MULTIPLE = 86000000;

    public function __construct()
    {
        $this->userRepo     =   app(UserRepositoryInterface::class);
        $this->userRoles    =   app(UserRolesRepositoryInterface::class); 

    }
    public function login(Request $request)
    {
        $this->validate($request, ['email' => 'required',
                                    'password' => 'required' ]);

        $credentials = $request->only(['email', 'password']);
        $token = app('auth')->attempt($credentials);

        if(!$token) {
            $data = ['error' => trans('messages.UNAUTHORIZED_ACCESS')];
        }else{
            $this->statusCode = Response::HTTP_OK;
            $this->data = [
                'token' => $token,
                'expires_in' => Auth::guard()->factory()->getTTL() * self::EXPIRY_TIME_MULTIPLE
            ];
            $this->message = trans('messages.LOGIN_SUCCESS');
        }

        return $this->sendResponse($this->statusCode, $this->data, $this->message);
    }

    public function logout()
    {
        app('auth')->logout();
        return $this->sendResponse(Response::HTTP_OK, [], trans('messages.LOGOUT_SUCCESS'));
    }

    public function register(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'name' => 'required',
            'phone' => 'required',
            'role_id'  =>  'required',
            'username' => 'required'
        ]);

        if($validator->fails())
        {
            $this->statusCode = Response::HTTP_UNPROCESSABLE_ENTITY;
            $this->data = ['error'  => $validator->errors()];
            $this->message = trans('messages.VALIDATION_ERROR');

        }else{
            $currentUser = app('auth')->user();
            $request->merge(['created_by' => $currentUser->id]);

            $user = $this->userRepo->create($request->only(['name','email','password','phone','username','created_by']))->toArray();

            $user['role'] = $this->userRoles->create(['role_id' => $request->role_id, 'user_id' => $user['id']]);

            $this->statusCode = Response::HTTP_CREATED;
            $this->message = trans('messages.USER_CREATED');
            $this->data = ['user' => $user];
        }

        return $this->sendResponse($this->statusCode, $this->data, $this->message);
    }

    public function role()
    {
        return "here is role!!!";
    }

}
?>