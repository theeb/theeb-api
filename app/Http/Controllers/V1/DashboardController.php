<?php
namespace App\Http\Controllers\V1;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Contracts\V1\UserRepositoryInterface;
use App\Repositories\Contracts\V1\CustomerRequestsRepositoryInterface;
use Validator;
use App\Models\V1\User;
use App\Models\V1\CustomerRequests;
use Illuminate\Http\Response;

class DashboardController extends Controller
{
    private $userRepo = null;
    private $customerRepo = null;
    public function __construct()
    {
        $this->userRepo = app(UserRepositoryInterface::class);
        $this->customerRepo = app(CustomerRequestsRepositoryInterface::class);
    }

    public function dashboard()
    {
        $currentUser = app('auth')->user();
        $userRole   =   \App\Models\V1\User::getUserRole($currentUser->id);

        switch ($userRole->group_slug) {
            case 'customer':
                # Customer Dashboard
                break;
            case 'admin':
                # Admin Dashboard
                $users = $this->userRepo->getListInAdminDashboard();
                $totalCustomerRequests = $this->customerRepo->countWhere([
                    ['current_status', '!=', null]
                ]);
                $this->data['dashboard'] = [
                    'users' =>  $users,
                    'total_customer_requests' => $totalCustomerRequests,
                ];
                $this->message = trans('messages.DASHBOARD_CONTENT');
                $this->statusCode = Response::HTTP_OK;
                break;
            case 'agent':
                # Agent Dashboard
                break;
            case 'driver':
                # Driver Dashboard
                break;
            default:
                break;
        }

        return $this->sendResponse($this->statusCode, $this->data, $this->message);
    }

}
?>