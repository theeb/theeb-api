<?php 
namespace App\Http\Controllers\V1;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Contracts\V1\UserRepositoryInterface;
use App\Repositories\Contracts\V1\UserRolesRepositoryInterface;
use Validator;
use App\Models\V1\User;
use Illuminate\Http\Response;

class UserController extends Controller
{
    private $userRepo;
    private $userRoles;

    public function __construct()
    {
        $this->userRepo     =   app(UserRepositoryInterface::class);
        $this->userRoles    =   app(UserRolesRepositoryInterface::class); 
    }

    public function profile()
    {
        return $this->sendResponse(Response::HTTP_OK, ['profile' => Auth::user()->toArray()], trans('messages.PROFILE_LISTED'));
    }

    public function updateUser(Request $request, $id = null)
    {
        $validator = Validator::make($request->all(),[
            'email'     =>  'required|email',
            'role_id'   =>  'required',
            'phone'     =>  'required',
            'username'  =>  'required'
        ]);
        if($validator->fails() || empty($id) )
        {
            $this->statusCode   = Response::HTTP_UNPROCESSABLE_ENTITY;
            $this->data         = ['error'  => $validator->errors()];
            $this->message      = trans('messages.VALIDATION_ERROR');
        }else{
            $update = $this->userRepo->update($id, $request->only(['name', 'last_name', 'phone', 'username', 'password', 'email' ]));

            $userRole = $this->userRoles->updateWhere(['user_id' => $id], ['role_id' => $request->role_id]);

            $this->statusCode = Response::HTTP_OK;
            $this->message = $update == true ? trans('messages.USER_UPDATED') : trans('messages.USER_UPDATE_FAILURE');
            $this->data['update'] = $update;
        }
        return $this->sendResponse($this->statusCode, $this->data, $this->message);
    }

    public function deleteUser(Request $request, $id = null)
    {        
        $currentUser = app('auth')->user();
        if($currentUser->id != $id && !empty($id) ){
            $delete = $this->userRepo->delete($id);
            $this->statusCode = Response::HTTP_OK;
            $this->message = $delete == true ? trans('messages.USER_DELETED') : trans('messages.USER_DELETE_FAILURE');
            $this->data['deleted'] = $delete;
        }else{
            $this->statusCode = Response::HTTP_UNPROCESSABLE_ENTITY;
            $this->data['error'] = trans('messages.VALIDATION_ERROR');
            $this->message = trans('messages.VALIDATION_ERROR');
        }
        
        return $this->sendResponse($this->statusCode, $this->data, $this->message);
    }

    public function listUsers(Request $request, $group = null)
    {
        $users = $this->userRepo->model()->with('userRole.role.groups')->whereHas('userRole.role.groups', function($query) use ($group){
            $query->where('user_groups.slug', $group);
        })->paginate(config('constants.PAGINATION_PER_PAGE'));

        $message = trans('messages.NO_USERS_FOUND');
        if($users->count()){
            $message = trans('messages.USERS_LISTED');
        }

        return $this->sendResponse(Response::HTTP_OK, ['users' => $users], $message);
    }
}
?>