<?php 
namespace App\Http\Controllers\V1;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Contracts\V1\CustomerRequestsRepositoryInterface;
use Validator;
use App\Models\V1\CustomerRequests;
use Illuminate\Http\Response;

class CustomerRequestController extends Controller
{
    private $customerRequestRepo;
    public function __construct()
    {
        $this->customerRequestRepo = app(CustomerRequestsRepositoryInterface::class);
    }

    public function customerRequest(Request $request)
    {
        $currentUser = app('auth')->user();
        print_r( $currentUser->toArray() ); die;
        // $this->data['customer_request'] = $currentUser->toArray();
        $this->sendResponse($this->statusCode, $this->data, $this->message);
    }

}
?>