<?php

namespace App\Models\V1;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract, AuthorizableContract ,JWTSubject
{
    use Authenticatable, Authorizable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','password','phone','username','created_by','last_name'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'deleted_at'
    ];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
    */
    
    protected $dates = ['deleted_at'];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = app('hash')->make($value);
    }
    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

     /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }   

    public static function getUserRole($userId = null, $status = 1)
    {
        if ( empty($userId) )
            return false;

        return \DB::table('users')
            ->select('users_roles.role_id', 'roles.name as role_name', 'user_groups.name as group_name', 'user_groups.slug as group_slug')
            ->join('users_roles', 'users.id', 'users_roles.user_id')
            ->join('roles', 'roles.id', 'users_roles.role_id')
            ->join('user_groups', 'user_groups.id', 'roles.group_id')
            ->where(['users.id' => $userId, 'users.status' => $status])
            ->first();
    }

    public function userRole()
    {
        return $this->hasOne('App\Models\V1\UsersRoles', 'user_id');
    }

}
