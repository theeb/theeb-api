<?php
namespace App\Models\V1;

use Illuminate\Database\Eloquent\Model;

class Resources extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id', 'route_name', 'route_as', 'route_controller'
    ];
}
?>
