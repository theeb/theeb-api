<?php 
namespace App\Models\V1;

use Illuminate\Database\Eloquent\Model;

class CustomerRequests extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'request_type', 'vehicle_type', 'vehicle_model'
    ];
}
?>
