<?php
namespace App\Models\V1;

use Illuminate\Database\Eloquent\Model;

class RolePermissions extends Model
{    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id', 'permission_id'
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

}
?>
