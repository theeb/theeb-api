<?php
namespace App\Models\V1;

use Illuminate\Database\Eloquent\Model;

class UserGroups extends Model
{
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function roles()
    {
        return $this->hasOne('App\Models\V1\Roles');
    }
}
?>
