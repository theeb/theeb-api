<?php
namespace App\Models\V1;

use Illuminate\Database\Eloquent\Model;


class Roles extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
    */
    protected $fillable = [
        'name', 'slug', 'group_id'
    ];
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at', 'slug'
    ];

    public function groups()
    {
        return $this->belongsTo('App\Models\V1\UserGroups', 'group_id');
    }

}
?>
