<?php
namespace App\Models\V1;

use Illuminate\Database\Eloquent\Model;

class Permissions extends Model
{    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

}
?>
