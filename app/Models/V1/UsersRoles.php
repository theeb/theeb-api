<?php
namespace App\Models\V1;

use Illuminate\Database\Eloquent\Model;


class UsersRoles extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
    */
    protected $fillable = [
        'role_id', 'user_id'
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function role()
    {
        return $this->belongsTo('App\Models\V1\Roles', 'role_id');
    }


}
?>
