<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Eloquent\V1\UserRepository;
use App\Repositories\Contracts\V1\UserRepositoryInterface;

use App\Repositories\Eloquent\V1\UserRolesRepository;
use App\Repositories\Contracts\V1\UserRolesRepositoryInterface;


use App\Repositories\Eloquent\V1\CustomerRequestsRepository;
use App\Repositories\Contracts\V1\CustomerRequestsRepositoryInterface;

use App\Repositories\Eloquent\V1\RoleRepository;
use App\Repositories\Contracts\V1\RoleRepositoryInterface;


class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(UserRolesRepositoryInterface::class, UserRolesRepository::class);
        $this->app->bind(CustomerRequestsRepositoryInterface::class, CustomerRequestsRepository::class);
        $this->app->bind(RoleRepositoryInterface::class, RoleRepository::class);
    }
}
