<?php
class Helpers
{

    public static function trimIfString($var)
    {
        return is_string($var) ? trim($var) : $var;
    }
}
?>